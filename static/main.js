var daysElement = document.querySelector('.timer-days')
var hoursElement = document.querySelector('.timer-hours')
var minutesElement = document.querySelector('.timer-minutes')
var secondsElement = document.querySelector('.timer-seconds')

setInterval(function() {
    var miseconds = (new Date(2019, 10, 24, 15, 30) - new Date())
    daysElement.textContent = Math.trunc(miseconds / 60000 / 60 / 24)
    hoursElement.textContent = Math.trunc(miseconds / 60000 / 60 % 24)
    minutesElement.textContent = Math.trunc(miseconds / 60000 % 60)
    secondsElement.textContent = Math.trunc(miseconds / 1000 % 60)
    }, 1000)